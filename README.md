# AngularApp#

Aplikacja wyświetlająca listę repozytoriów z serwisu Github, a także użytkowników i ich profile.

### Co znajduje się w repozytorium ? ###

* Cały kod aplikacji, pozwalający na jej uruchomienie

### Wykorzystane technologie ###

* Angular 2
* Bootstrap
* CSS 3
* Github API