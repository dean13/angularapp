import { Component } from '@angular/core';
import { Router, ROUTER_DIRECTIVES, ActivatedRoute } from '@angular/router';
import {PostService} from './post.service';
import {Http,HTTP_PROVIDERS,Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Component({
    templateUrl: 'app/src/information.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [PostService, HTTP_PROVIDERS],
    precompile: [InformationComponent]
})
export class InformationComponent {
    repo = [];
    information = [];
    user = '';

    constructor(private route: ActivatedRoute, private _postService:PostService) {
        this.route.params.subscribe(params => {
            this.user = params['name'];
        });

        this.nngOnInit();
    }

    nngOnInit() {
        Observable.forkJoin(
            this._postService.getUser(this.user),
            this._postService.getAllRepo(this.user)
        ).subscribe(
            res => {
                this.information = res[0];
                this.repo = res[1];
            }
        );
    }
}
