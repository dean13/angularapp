var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var post_service_1 = require('./post.service');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
var CustomPipe_1 = require("./CustomPipe");
var HomeComponent = (function () {
    function HomeComponent(_postService) {
        this._postService = _postService;
        this.username = "angular";
        this.user = '';
        this.repository = [];
        this.test = [];
        this.nngOnInit();
    }
    HomeComponent.prototype.nngOnInit = function () {
        var _this = this;
        Rx_1.Observable.forkJoin(this._postService.getAllRepo(this.username)).subscribe(function (res) {
            _this.repository = res[0].sort(_this.GetSortOrder('watchers'));
        });
    };
    HomeComponent.prototype.GetSortOrder = function (prop) {
        return function (a, b) {
            if (a[prop] < b[prop]) {
                return 1;
            }
            else if (a[prop] > b[prop]) {
                return -1;
            }
            return 0;
        };
    };
    HomeComponent = __decorate([
        core_1.Component({
            template: "\n        <div class=\"panel\">\n            <div>\n                <ul class=\"list-group content\">\n            <li *ngFor=\"let repo of repository\" class=\"list-group-item\">\n                <h1><a routerLink=\"repository/{{repo.name}}\">{{repo.name}}</a>\n                <div class=\"right\" style=\"font-size: 0.6em\">\n                {{repo.watchers | number}}<span class=\"glyphicon glyphicon-eye-open\"></span>\n                {{repo.forks | number}}<span class=\"glyphicon glyphicon-user\"></span>\n                </div></h1>\n            </li>\n        </ul>\n            </div>\n        </div>\n        ",
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [post_service_1.PostService, http_1.HTTP_PROVIDERS],
            pipes: [CustomPipe_1.CustomPipe],
            precompile: [HomeComponent],
        }), 
        __metadata('design:paramtypes', [post_service_1.PostService])
    ], HomeComponent);
    return HomeComponent;
})();
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map