import { Component } from '@angular/core';
import { Router, ROUTER_DIRECTIVES, ActivatedRoute } from '@angular/router';
import {PostService} from './post.service';
import {Http,HTTP_PROVIDERS,Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Component({
    templateUrl: 'app/src/repository.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [PostService, HTTP_PROVIDERS],
    precompile: [RepositoryComponent]
})
export class RepositoryComponent {
    repository = '';
    data = [];
    content_readme = '';
    user = 'angular';
    contributors = [];

    constructor(private route: ActivatedRoute, private _postService:PostService) {
        this.route.params.subscribe(params => {
            this.repository = params['name'];
        });

        this.nngOnInit();
    }

    nngOnInit() {
        Observable.forkJoin(
            this._postService.getRepo(this.user, this.repository),
            this._postService.getContributors(this.user, this.repository)
        ).subscribe(
            res => {
                this.data = res[0];
                this.contributors = res[1];
            }
        );
    }
}
