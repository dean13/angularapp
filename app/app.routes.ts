import { provideRouter, RouterConfig } from '@angular/router';
import { InformationComponent }  from './information.component';
import { RepositoryComponent }    from './repository.component';
import {HomeComponent} from "./home.component";
import {ContributorsComponent} from "./contributors.component";
import {FollowersComponent} from "./followers.component";
const routes: RouterConfig = [
    { path: '', component: HomeComponent },
    { path: 'information/:name', component: InformationComponent },
    { path: 'repository/:name',component: RepositoryComponent },
    { path: 'contributors',component: ContributorsComponent },
    { path: 'followers',component: FollowersComponent }
];
export const appRouterProviders = [
    provideRouter(routes)
];
