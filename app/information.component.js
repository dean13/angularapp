var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var post_service_1 = require('./post.service');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
var InformationComponent = (function () {
    function InformationComponent(route, _postService) {
        var _this = this;
        this.route = route;
        this._postService = _postService;
        this.repo = [];
        this.information = [];
        this.user = '';
        this.route.params.subscribe(function (params) {
            _this.user = params['name'];
        });
        this.nngOnInit();
    }
    InformationComponent.prototype.nngOnInit = function () {
        var _this = this;
        Rx_1.Observable.forkJoin(this._postService.getUser(this.user), this._postService.getAllRepo(this.user)).subscribe(function (res) {
            _this.information = res[0];
            _this.repo = res[1];
        });
    };
    InformationComponent = __decorate([
        core_1.Component({
            templateUrl: 'app/src/information.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [post_service_1.PostService, http_1.HTTP_PROVIDERS],
            precompile: [InformationComponent]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, post_service_1.PostService])
    ], InformationComponent);
    return InformationComponent;
})();
exports.InformationComponent = InformationComponent;
//# sourceMappingURL=information.component.js.map