import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, ActivatedRoute } from '@angular/router';
import {PostService} from './post.service';
import {Http,HTTP_PROVIDERS,Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {CustomPipe} from "./CustomPipe";

@Component({
    template: `
        <div class="panel">
            <div>
                <ul class="list-group content">
            <li *ngFor="let repo of repository" class="list-group-item">
                <h1><a routerLink="repository/{{repo.name}}">{{repo.name}}</a>
                <div class="right" style="font-size: 0.6em">
                {{repo.watchers | number}}<span class="glyphicon glyphicon-eye-open"></span>
                {{repo.forks | number}}<span class="glyphicon glyphicon-user"></span>
                </div></h1>
            </li>
        </ul>
            </div>
        </div>
        `,
    directives: [ROUTER_DIRECTIVES],
    providers: [PostService, HTTP_PROVIDERS],
    pipes: [CustomPipe],
    precompile: [FollowersComponent],
})
export class FollowersComponent {
    username = "angular";
    user = '';
    repository = [];
    test = [];

    constructor(private _postService:PostService) {
        this.nngOnInit();
    }

    nngOnInit() {
        Observable.forkJoin(
            this._postService.getAllRepo(this.username)
        ).subscribe(
            res => {
                this.repository = res[0].sort(this.GetSortOrder('watchers'));
            }
        );
    }

    GetSortOrder(prop) {
        return function(a, b) {
            if (a[prop] < b[prop]) {
                return 1;
            } else if (a[prop] > b[prop]) {
                return -1;
            }
            return 0;
        }
    }



}
