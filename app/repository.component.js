var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var post_service_1 = require('./post.service');
var http_1 = require('@angular/http');
var Rx_1 = require('rxjs/Rx');
var RepositoryComponent = (function () {
    function RepositoryComponent(route, _postService) {
        var _this = this;
        this.route = route;
        this._postService = _postService;
        this.repository = '';
        this.data = [];
        this.content_readme = '';
        this.user = 'angular';
        this.contributors = [];
        this.route.params.subscribe(function (params) {
            _this.repository = params['name'];
        });
        this.nngOnInit();
    }
    RepositoryComponent.prototype.nngOnInit = function () {
        var _this = this;
        Rx_1.Observable.forkJoin(this._postService.getRepo(this.user, this.repository), this._postService.getContributors(this.user, this.repository)).subscribe(function (res) {
            _this.data = res[0];
            _this.contributors = res[1];
        });
    };
    RepositoryComponent = __decorate([
        core_1.Component({
            templateUrl: 'app/src/repository.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [post_service_1.PostService, http_1.HTTP_PROVIDERS],
            precompile: [RepositoryComponent]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, post_service_1.PostService])
    ], RepositoryComponent);
    return RepositoryComponent;
})();
exports.RepositoryComponent = RepositoryComponent;
//# sourceMappingURL=repository.component.js.map