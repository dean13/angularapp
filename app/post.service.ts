import {Http,HTTP_PROVIDERS,Response,Headers} from "@angular/http";
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class PostService {
    private _url = "https://api.github.com/";

    constructor(private _http: Http) {

    }

    getUser(username) {
        return this._http.get(this._url + 'users/' + username)
            .map(res => res.json());
    }

    getFollowers(username) {
        return this._http.get(this._url + 'users/' + username + "/followers")
            .map(res => res.json());
    }

    getRepo(username, repository) {
        return this._http.get(this._url + 'repos/' + username + '/' + repository)
            .map(res => res.json());
    }

    getAllRepo(username) {
        return this._http.get(this._url + 'users/' + username + '/repos')
            .map(res => res.json());
    }

    getContributors(username, repository) {
        return this._http.get(this._url + 'repos/' + username + '/' + repository + '/contributors')
            .map(res => res.json());
    }

}