var router_1 = require('@angular/router');
var information_component_1 = require('./information.component');
var repository_component_1 = require('./repository.component');
var home_component_1 = require("./home.component");
var contributors_component_1 = require("./contributors.component");
var followers_component_1 = require("./followers.component");
var routes = [
    { path: '', component: home_component_1.HomeComponent },
    { path: 'information/:name', component: information_component_1.InformationComponent },
    { path: 'repository/:name', component: repository_component_1.RepositoryComponent },
    { path: 'contributors', component: contributors_component_1.ContributorsComponent },
    { path: 'followers', component: followers_component_1.FollowersComponent }
];
exports.appRouterProviders = [
    router_1.provideRouter(routes)
];
//# sourceMappingURL=app.routes.js.map