var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") return Reflect.decorate(decorators, target, key, desc);
    switch (arguments.length) {
        case 2: return decorators.reduceRight(function(o, d) { return (d && d(o)) || o; }, target);
        case 3: return decorators.reduceRight(function(o, d) { return (d && d(target, key)), void 0; }, void 0);
        case 4: return decorators.reduceRight(function(o, d) { return (d && d(target, key, o)) || o; }, desc);
    }
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var http_1 = require("@angular/http");
var core_1 = require('@angular/core');
require('rxjs/add/operator/map');
var PostService = (function () {
    function PostService(_http) {
        this._http = _http;
        this._url = "https://api.github.com/";
    }
    PostService.prototype.getUser = function (username) {
        return this._http.get(this._url + 'users/' + username)
            .map(function (res) { return res.json(); });
    };
    PostService.prototype.getFollowers = function (username) {
        return this._http.get(this._url + 'users/' + username + "/followers")
            .map(function (res) { return res.json(); });
    };
    PostService.prototype.getRepo = function (username, repository) {
        return this._http.get(this._url + 'repos/' + username + '/' + repository)
            .map(function (res) { return res.json(); });
    };
    PostService.prototype.getAllRepo = function (username) {
        return this._http.get(this._url + 'users/' + username + '/repos')
            .map(function (res) { return res.json(); });
    };
    PostService.prototype.getContributors = function (username, repository) {
        return this._http.get(this._url + 'repos/' + username + '/' + repository + '/contributors')
            .map(function (res) { return res.json(); });
    };
    PostService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], PostService);
    return PostService;
})();
exports.PostService = PostService;
//# sourceMappingURL=post.service.js.map